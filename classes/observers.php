<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local_new_user
 * @copyright  2015 Smith College
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_new_user;

defined('MOODLE_INTERNAL') || die();

global $CFG;

include_once ($CFG->dirroot . '/user/lib.php');
include_once ($CFG->dirroot . '/lib/classes/user.php');

class observers {

    /**
     * New user added
     *
     * @param \core\event\user_created $event
     * @return void
     */
    public static function user_cleanup($event) {
        global $DB;

        $newuserid = $event->relateduserid;
        $newuser = \core_user::get_user($newuserid);

        if ( $newuser->auth == 'db' ) {

            $conditions = array ( 'idnumber' => $newuser->idnumber );

            $record = new \stdClass();
            $record->id = $newuserid;
            $record->auth = 'shibboleth';
            $DB->update_record ( 'user' , $record );

            $recordscount = $DB->count_records('user', $conditions);

            if ( $recordscount > 1 ) {
                $deleteconditions = array ( 'idnumber' => $newuser->idnumber , 'username' => $newuser->username );
                $DB->delete_records ( 'user' , $deleteconditions );
                $oldid = $DB->get_field('user', 'id', $conditions);
                $record = new \stdClass();
                $record->id = $oldid;
                $record->username = $newuser->username;
                $record->firstname = $newuser->firstname;
                $record->lastname = $newuser->lastname;
                $record->email = $newuser->email;
                $DB->update_record ( 'user' , $record );
            }

        }

        return true;
    }
}
